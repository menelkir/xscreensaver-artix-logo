xscreensaver-artix-logo
=======================

An artix version for xscreensaver-arch-logo

===========
Themes used
===========

:xscreensaver-arch-logo: https://aur.archlinux.org/packages/xscreensaver-arch-logo/

=============
How to donate
=============

If you find this repo useful (don't forget to pay a visit to the related
repos too), you can buy me a beer:

:BTC: 3ECzX5UhcFSRv6gBBYLNBc7zGP9UA5Ppmn

:ETH: 0x7E17Ac09Fa7e6F80284a75577B5c1cbaAD566C1c

